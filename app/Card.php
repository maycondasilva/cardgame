<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Card extends Model
{
  /**
   * The attributes that are mass assignable.
   *
   * @var array
   */
  protected $fillable = [
      'imagem', 'nome', 'tema', 'frase', 'descricao',
  ];
}
