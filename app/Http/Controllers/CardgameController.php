<?php

namespace App\Http\Controllers;

use App\Repositories\CardRepository;
use Illuminate\Http\Request;

class CardgameController extends Controller
{

  /**
   * Create a new controller instance.
   * @param CardRepository $cardRepository
   * @return void
   */
  public function __construct(CardRepository $cardRepository)
  {
      $this->middleware('auth');
      $this->cardRepository = $cardRepository;
  }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      $cards = $this->cardRepository->all();
      return view('cardgame.home', compact('cards'));
    }

    /**
     * Show the application adicionar.
     *
     * @return \Illuminate\Http\Response
     */
    public function adicionar()
    {
      return view('cards.adicionar');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      $data = $request->all();
      if($request->hasfile('imagem') && $request->file('imagem')->isValid()){
        $upload = $request->imagem->store('cards');
        if (!$upload) {
          return redirect()->back()->with('error','Falha ao fazer upload da imagem');
        }
        $data['imagem'] = $upload;
        $resposta = $this->cardRepository->create($data);
        return redirect()->route('cardgame.home');
      }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
      $card = $this->cardRepository->find($id);
      return view('cards.editar', compact('card'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
      $idCard = $id;
      $dados = $request->all();
      if($request->hasfile('imagem') && $request->file('imagem')->isValid()){
        $upload = $request->imagem->store('cards');
        if (!$upload) {
          return redirect()->back()->with('error','Falha ao fazer upload da imagem');
        }
        $dados['imagem'] = $upload;
        $resposta = $this->cardRepository->atualizar($idCard, $dados);
        return redirect()->route('cardgame.home');
      }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
      $card = $this->cardRepository->delete($id);
      return redirect()->route('cardgame.home');
    }
}
