<?php

namespace App\Repositories;

use Illuminate\Support\Facades\DB;
use App\Card;
use Illuminate\Support\Facades\Storage;

class CardRepository
{
  /**
   * @var Card
   */
  protected $card;

  /**
   * CardRepository constructor.
   *
   */
  public function __construct(Card $card)
  {
    $this->card = $card;
  }

  /**
   * Display the specified resource.
   * Retorna todas as cards
   * @return json_decode retorna a pesquisa para o CardgameController;
   */
  public function all()
  {
    $response = $this->card->all();
    return json_decode($response);
  }

  /**
   * É feito a inserção do dado no banco de dados
   * @param  int  $data
   * @return $card retorna o registro para o CardgameController;
   */
  public function create($data)
  {
    $card = $this->card->create($data);
    return $card;
  }

  /**
   * Display the specified resource.
   * Faço uma pesquisa no banco de dados, buscando a card pelo ID
   * @param  int  $id
   * @return json_decode retorna a pesquisa para o CardgameController;
   */
  public function find($id)
  {
    $response = $this->card->find($id);
    return json_decode($response);
  }

  /**
   * Atualiza os dados do card.
   *
   * @param $idCard, $dados
   */
  public function atualizar($idCard, $dados)
  {
    $card = $this->card->find($idCard)->update($dados);
  }

  /**
   * Display the specified resource.
   * Deleta o card pelo id
   * @param  int  $id
   * @return json_decode retorna a pesquisa para o CardgameController;
   */
  public function delete($id)
  {
    $response = $this->card->find($id);
    if (isset($response)) {
        Storage::disk('public')->delete($response->imagem);
        $response->delete();
    }
  }
}
