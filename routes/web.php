<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();
Route::get('/',['as'=>'cardgame.home', 'uses'=>'HomeController@index']);
Route::get('/',['as'=>'cardgame.home', 'uses'=>'CardgameController@index']);

Route::get('card/adicionar',['as'=>'cards.adicionar', 'uses'=>'CardgameController@adicionar']);
Route::post('/',['as'=>'cards.salvar', 'uses'=>'CardgameController@store']);
Route::get('card/{id}',['as'=>'cards.editar', 'uses'=>'CardgameController@show']);
Route::put('/card/atualizar/{id}',['as'=>'cards.atualizar', 'uses'=>'CardgameController@update']);
Route::get('/card/delete/{id}',['as'=>'cards.delete', 'uses'=>'CardgameController@destroy']);

Route::get('perfil/{id}',['as'=>'user.perfil', 'uses'=>'UserController@edit']);
Route::put('perfil/update/{id}',['as'=>'user.update', 'uses'=>'UserController@update']);
