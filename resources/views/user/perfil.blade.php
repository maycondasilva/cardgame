@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Meu Perfil: {{ Auth::user()->name }}</div>

                <div class="panel-body">
                    <form class="form-horizontal" action="{{route('user.update', $registro->id)}}" method="post">
                        {{ csrf_field() }}
                        <input type="hidden" name="_method" value="put">

                        <div class="form-group{{ $errors->has('nome') ? ' has-error' : '' }}">
                            <label for="nome" class="col-md-4 control-label">Nome</label>
                            <div class="col-md-6">
                              <input id="name" type="text" class="form-control" name="name" value="{{isset($registro->name) ? $registro->name : ''}}" autofocus>
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('frase') ? ' has-error' : '' }}">
                            <label for="frase" class="col-md-4 control-label">E-mail Adress</label>
                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control" name="email" value="{{isset($registro->email) ? $registro->email : ''}}" disabled>
                            </div>
                        </div>

                        <div class="row">
                          <div class="col-md-9 col-sm-6">
                            <button type="button" id="voltar" class="btn btn-primary ">Voltar</button>
                          </div>

                          <div class="col-md-3 col-sm-6">
                            <button type="submit" class="btn btn-success btn pull-right">Atualizar</button>
                          </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
