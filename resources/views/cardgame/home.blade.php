@extends('layouts.app')

@section('content')
<div class="container">
  <div class="row">
    <div class="col-md-12">
      @if (session('alert'))
        <div class="col-md-12 alert alert-danger alert-block">
            {{ session('alert') }}
        </div>
      @endif
      @if(isset($errors) && count($errors) >0)
        <div class="col-md-12 alert alert-danger alert-block">
          @foreach($errors->all() as $error)
            <p>{{$error}}</p>
          @endforeach
        </div>
      @endif

      @if(isset($cards) == true && count($cards) > 0)
      <div class="row">
        @foreach($cards as $card)
          <div class="col-md-4">
            <a href="/card/{{ $card->id }}">
              <div class="card" style="background-color: #EAEAEA;  text-align:center">
                <img class="card-img-top" src="{{ url('storage/'.$card->imagem)}}" alt="Card image" style="height:250px; width:150px">
                <div class="card-body" style="background-color: #FFFFFF">
                  <h4 class="card-title">{{$card->nome}}</h4>
                  <p class="card-text">{{$card->descricao}}</p>
                </div>
              </div>
            </a>
          </div>
        @endforeach
      </div>
      @endif
    </div>
  </div>

</div>
<a href="{{route('cards.adicionar')}}" type="button" class="btn btn-primary" style="position: fixed;bottom: 40px;right: 40px;">Add Card</a>
@endsection
