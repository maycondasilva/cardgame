@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Editar Card</div>

                <div class="panel-body">
                    <form class="form-horizontal" method="POST" action="{{route('cards.atualizar', $card->id)}}" enctype="multipart/form-data">
                      <input type="hidden" name="_method" value="put">
                        {{ csrf_field() }}

                        <div class="form-group{{ $errors->has('nome') ? ' has-error' : '' }}">
                            <label for="nome" class="col-md-4 control-label">Nome</label>
                            <div class="col-md-6">
                                <input id="nome" type="text" class="form-control" name="nome"  value="{{isset($card->nome) ? $card->nome : ''}}" required autofocus>
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('frase') ? ' has-error' : '' }}">
                            <label for="frase" class="col-md-4 control-label">Frase</label>
                            <div class="col-md-6">
                                <input id="frase" type="frase" class="form-control" name="frase" value="{{isset($card->frase) ? $card->frase : ''}}" required>
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('descricao') ? ' has-error' : '' }}">
                            <label for="descricao" class="col-md-4 control-label">Descrição</label>
                            <div class="col-md-6">
                                <input id="descricao" type="descricao" class="form-control" name="descricao" value="{{isset($card->descricao) ? $card->descricao : ''}}" required>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="tema" class="col-md-4 control-label">Tema</label>
                            <div class="col-md-6">
                                <input id="tema" type="tema" class="form-control" name="tema" value="{{isset($card->tema) ? $card->tema : ''}}" required>
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('imagem') ? ' has-error' : '' }}">
                          @if($card->imagem != null)
                          <img class="card-img-top" src="{{ url('storage/'.$card->imagem)}}" alt="Card image" style="height:250px; width:150px">
                          @endif
                            <label for="imagem" class="col-md-4 control-label">Imagem</label>
                            <div class="col-md-6">
                                <input id="imagem" type="file" class="form-control" name="imagem" value="{{ old('imagem') }}">
                            </div>
                        </div>

                        <div class="row">
                          <div class="col-md-4">
                            <button type="button" id="voltar" class="btn btn-primary ">Voltar</button>
                          </div>

                          <div class="col-md-4 text-center">
                            <a href="{{route('cards.delete', $card->id)}}" type="button" class="btn btn-danger center-block">Excluir Card</a>
                          </div>

                          <div class="col-md-4">
                            <button type="submit" class="btn btn-success btn pull-right">Atualizar</button>
                          </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
