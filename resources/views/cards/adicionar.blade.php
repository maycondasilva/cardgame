@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Cadastrar Card</div>

                <div class="panel-body">
                    <form class="form-horizontal" method="POST" action="{{ route('cards.salvar')}}" enctype="multipart/form-data">
                        {{ csrf_field() }}

                        <div class="form-group{{ $errors->has('nome') ? ' has-error' : '' }}">
                            <label for="nome" class="col-md-4 control-label">Nome</label>
                            <div class="col-md-6">
                                <input id="nome" type="text" class="form-control" name="nome" value="{{ old('nome') }}" required autofocus>
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('frase') ? ' has-error' : '' }}">
                            <label for="frase" class="col-md-4 control-label">Frase</label>
                            <div class="col-md-6">
                                <input id="frase" type="frase" class="form-control" name="frase" value="{{ old('frase') }}" required>
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('descricao') ? ' has-error' : '' }}">
                            <label for="descricao" class="col-md-4 control-label">Descrição</label>
                            <div class="col-md-6">
                                <input id="descricao" type="descricao" class="form-control" name="descricao" value="{{ old('descricao') }}" required>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="tema" class="col-md-4 control-label">Tema</label>
                            <div class="col-md-6">
                                <input id="tema" type="tema" class="form-control" name="tema" value="{{ old('tema') }}" required>
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('imagem') ? ' has-error' : '' }}">
                            <label for="imagem" class="col-md-4 control-label">Imagem</label>
                            <div class="col-md-6">
                                <input id="imagem" type="file" class="form-control" name="imagem" value="{{ old('imagem') }}" required>
                            </div>
                        </div>

                        <div class="row">
                          <div class="col-md-9 col-sm-6">
                            <button type="button" id="voltar" class="btn btn-primary ">Voltar</button>
                          </div>

                          <div class="col-md-3 col-sm-6">
                            <button type="submit" class="btn btn-success btn pull-right">Adicionar</button>
                          </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
