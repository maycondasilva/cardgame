-- --------------------------------------------------------
-- Servidor:                     127.0.0.1
-- Versão do servidor:           10.1.21-MariaDB - mariadb.org binary distribution
-- OS do Servidor:               Win32
-- HeidiSQL Versão:              10.1.0.5464
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Copiando estrutura do banco de dados para cardgame
CREATE DATABASE IF NOT EXISTS `cardgame` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `cardgame`;

-- Copiando estrutura para tabela cardgame.cards
CREATE TABLE IF NOT EXISTS `cards` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `imagem` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tema` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nome` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `frase` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `descricao` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Copiando dados para a tabela cardgame.cards: ~6 rows (aproximadamente)
/*!40000 ALTER TABLE `cards` DISABLE KEYS */;
INSERT INTO `cards` (`id`, `imagem`, `tema`, `nome`, `frase`, `descricao`, `created_at`, `updated_at`) VALUES
	(14, 'cards/d6SGTi0hYzOHCRGqPZ5inekJPhKsYYPPNFuvuhky.png', 'YIGIOH', 'YUBEL - TERROR INCARNATE', 'PTDN-EN007', 'THIS CARD CANNOT BE NORMAL SUMMONED', '2019-07-25 03:46:34', '2019-07-25 03:46:34'),
	(16, 'cards/oKInkIY0tirzFBOpezqRz36rzySVXmtYjTi4HHB3.jpeg', 'YIGIOH', 'BABY DRAGON', 'LCJW-EN006', 'MUTH MORE THAN JUST A CHILD', '2019-07-25 03:50:34', '2019-07-25 05:29:22'),
	(18, 'cards/HKKibuSL8mdJhAVSllyDJOHXGYgMNg2l2z67Mzv6.jpeg', 'YIGIOH', 'MALDIÇÃO DE ANUBIS', 'EP03-FT199', 'COLOQUE TODOS OS MONSTROS DE EFEITO NO CAMPO EM POSIÇÃO DE DEFESA', '2019-07-25 03:52:42', '2019-07-25 03:52:42'),
	(19, 'cards/g9eO9ce2T8g3TqCbwQFj2q44Y8tHnBHRTfLQF3Mf.jpeg', 'YIGIOH', 'Dark Magician', 'DUSA-EN100', 'THE ULTIMATE WIZARD IN TERMS OF ATTACK AND DEFENSE', '2019-07-25 03:53:26', '2019-07-25 03:53:26'),
	(20, 'cards/YGFzFCC49DOcOIEKw3ir30YWmCyxokKmljTBvb6l.jpeg', 'YIGIOH', 'DRAGON TOON DE OJOS AZULES', 'BIP-S020', 'ESTA CARTA PODE SER INVOCADA APENAS SE O MUNDO TOON ESTIVER EM CAMPO', '2019-07-25 03:54:33', '2019-07-25 03:54:33');
/*!40000 ALTER TABLE `cards` ENABLE KEYS */;

-- Copiando estrutura para tabela cardgame.migrations
CREATE TABLE IF NOT EXISTS `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Copiando dados para a tabela cardgame.migrations: ~3 rows (aproximadamente)
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
	(1, '2014_10_12_000000_create_users_table', 1),
	(2, '2014_10_12_100000_create_password_resets_table', 1),
	(3, '2019_07_24_205601_create_cards_table', 2);
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;

-- Copiando estrutura para tabela cardgame.password_resets
CREATE TABLE IF NOT EXISTS `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Copiando dados para a tabela cardgame.password_resets: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `password_resets` DISABLE KEYS */;
/*!40000 ALTER TABLE `password_resets` ENABLE KEYS */;

-- Copiando estrutura para tabela cardgame.users
CREATE TABLE IF NOT EXISTS `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Copiando dados para a tabela cardgame.users: ~1 rows (aproximadamente)
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` (`id`, `name`, `email`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
	(1, 'Administrador', 'admin@admin.com', '$2y$10$g.w9lx0cLyymC38suLJTu.VxZghzpxJd.fPbr3pY4eHrKHVggCjMm', 'o2LoPxCnNpH0PlYzSwfAAcJtv8s6cbcXrIJXONmge4ogLG2xeVLBUnETjuUR', '2019-07-24 16:37:26', '2019-07-25 07:16:49');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
